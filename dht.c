#include <msp430.h>
#include "dht.h"


#define DATA_PIN BIT3
#define CTRL_PORT P1OUT
#define CTRL_DIR P1DIR
#define CTRL_IN P1IN


dht_data dht_read(void)
{
	CTRL_DIR |= DATA_PIN; //bit4 for host actions, bit5 for what host reads from sensor
	CTRL_PORT &= ~DATA_PIN; //start signal, low for 500us
	__delay_cycles(16000);
	CTRL_PORT |= DATA_PIN; //host pulls up voltage
	__delay_cycles(640);
	CTRL_DIR &= ~DATA_PIN; //host waits for sensor response
	
	while(CTRL_IN & DATA_PIN)  //host has released control of pin, wait for sensor to output low
	{
		if(CTRL_IN & DATA_PIN)
		{
			__delay_cycles(10);
		}
	}

	while(!(CTRL_IN&DATA_PIN)) //sensor response signal, low
	{
		if(!(CTRL_IN & DATA_PIN))
		{
			__delay_cycles(10);
		}
	}


	while(CTRL_IN & DATA_PIN) //sensor pulls up voltage
	{
		if(CTRL_IN & DATA_PIN)
		{
			__delay_cycles(10);
		}
	}

	//sensor begins sending data
	uint8_t data[5] = {0};

	int i, j;
	for(i=0; i<5; i++)
	{
		for(j=0; j<8; j++)
		{

			while(!(CTRL_IN & DATA_PIN))
			{
				if(!(CTRL_IN & DATA_PIN))
				{
					__delay_cycles(10);
				}
			}

			__delay_cycles(800); //wait 50usec
			if(CTRL_IN & DATA_PIN)
			{
				data[i] |= 1 << (7-j);
				__delay_cycles(480);
			}
		}
	}

	dht_data reading;
	reading.humidity = (data[0] << 8) | data[1];
	reading.temp = (data[2] << 8) | data[3];

	return reading;
}
