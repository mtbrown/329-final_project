/*
 * uart.h
 *
 *  Created on: May 19, 2016
 *      Author: Mark
 */

#ifndef UART_H_
#define UART_H_

void uart_init();
void uart_print(char *str);

#endif /* UART_H_ */
