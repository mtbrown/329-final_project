#include <msp430.h>
#include "uart.h"

void uart_init() {
	UCA0CTL0 = 0;
	UCA0CTL1 |= UCSWRST; // Put USCI in reset mode
	UCA0CTL1 = UCSSEL_2 | UCSWRST; // Use SMCLK, still reset
	UCA0MCTL = UCBRF_0 | UCBRS_6;
	UCA0BR0 = 65; // 9600 bauds
	UCA0BR1 = 3;
	UCA0CTL1 &= ~UCSWRST; // Normal mode
	P1SEL2 |= (BIT1 + BIT2); // Set pins for USCI
	P1SEL |= (BIT1 + BIT2);
}

void uart_print(char *str) {
	while(*str) {
		while (UCA0STAT & UCBUSY)
			/* wait */;
		UCA0TXBUF = *str++;
	}
}
