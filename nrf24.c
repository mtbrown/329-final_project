#include <msp430.h>
#include <stdint.h>

#define CTRL_PORT P2OUT
#define CTRL_DIR P2DIR
#define CSN_BIT BIT1
#define CE_BIT BIT0

#define RF_ADDR_WIDTH 5

/* Register Map */
#define RF24_CONFIG      0x00
#define RF24_EN_AA       0x01
#define RF24_EN_RXADDR   0x02
#define RF24_SETUP_AW    0x03
#define RF24_SETUP_RETR  0x04
#define RF24_RF_CH       0x05
#define RF24_RF_SETUP    0x06
#define RF24_STATUS      0x07
#define RF24_OBSERVE_TX  0x08
#define RF24_CD          0x09
#define RF24_RPD         0x09
#define RF24_RX_ADDR_P0  0x0A
#define RF24_RX_ADDR_P1  0x0B
#define RF24_RX_ADDR_P2  0x0C
#define RF24_RX_ADDR_P3  0x0D
#define RF24_RX_ADDR_P4  0x0E
#define RF24_RX_ADDR_P5  0x0F
#define RF24_TX_ADDR     0x10
#define RF24_RX_PW_P0    0x11
#define RF24_RX_PW_P1    0x12
#define RF24_RX_PW_P2    0x13
#define RF24_RX_PW_P3    0x14
#define RF24_RX_PW_P4    0x15
#define RF24_RX_PW_P5    0x16
#define RF24_FIFO_STATUS 0x17
#define RF24_DYNPD       0x1C
#define RF24_FEATURE     0x1D

/* Register Bits */
#define RF24_MASK_RX_DR  BIT6
#define RF24_MASK_TX_DS  BIT5
#define RF24_MASK_MAX_RT BIT4
#define RF24_EN_CRC      BIT3
#define RF24_CRCO        BIT2
#define RF24_PWR_UP      BIT1
#define RF24_PRIM_RX     BIT0
#define RF24_RX_DR       BIT6
#define RF24_TX_DS       BIT5
#define RF24_MAX_RT      BIT4

/* Instructions */
#define RF24_R_REGISTER    0x00
#define RF24_W_REGISTER    0x20
#define RF24_REGISTER_MASK 0x1F
#define RF24_R_RX_PAYLOAD  0x61
#define RF24_W_TX_PAYLOAD  0xA0
#define RF24_FLUSH_TX      0xE1
#define RF24_FLUSH_RX      0xE2
#define RF24_REUSE_TX_PL   0xE3
#define RF24_R_RX_PL_WID   0x60
#define RF24_W_ACK_PAYLOAD 0xA8
#define RF24_W_TX_PAYLOAD_NOACK 0xB0
#define RF24_NOP           0xFF

#define DELAY_CYCLES_5MS       80000
#define DELAY_CYCLES_130US     2080
#define DELAY_CYCLES_15US      240


uint8_t rf_status;  // stores the current state of the status register


void spi_init() {
	/* Configure ports on MSP430 device for USCI_B */
	P1SEL |= BIT5 | BIT6 | BIT7;
	P1SEL2 |= BIT5 | BIT6 | BIT7;

	/* USCI-B specific SPI setup */
	UCB0CTL1 |= UCSWRST;
	UCB0CTL0 = UCCKPH | UCMSB | UCMST | UCMODE_0 | UCSYNC;  // SPI mode 0, master
	UCB0BR0 = 0x01;  // SPI clocked at same speed as SMCLK
	UCB0BR1 = 0x00;
	UCB0CTL1 = UCSSEL_2;  // Clock = SMCLK, clear UCSWRST and enables USCI_B module.
}

uint8_t spi_transfer(uint8_t inb) {
	UCB0TXBUF = inb;
	while ( !(IFG2 & UCB0RXIFG) )  // Wait for RXIFG indicating remote byte received via SOMI
		;
	return UCB0RXBUF;
}

uint16_t spi_transfer16(uint16_t inw) {
	uint16_t retw;
	uint8_t *retw8 = (uint8_t *)&retw, *inw8 = (uint8_t *)&inw;

	UCB0TXBUF = inw8[1];
	while ( !(IFG2 & UCB0RXIFG) )
		;
	retw8[1] = UCB0RXBUF;
	UCB0TXBUF = inw8[0];
	while ( !(IFG2 & UCB0RXIFG) )
		;
	retw8[0] = UCB0RXBUF;
	return retw;
}

// Basic register I/O through SPI
uint8_t read_register(uint8_t addr) {
	uint16_t data;
	CTRL_PORT &= ~CSN_BIT;
	data = spi_transfer16(RF24_NOP | ((addr & RF24_REGISTER_MASK) << 8));
	rf_status = (uint8_t) ((data & 0xFF00) >> 8);
	CTRL_PORT |= CSN_BIT;
	return (uint8_t) (data & 0x00FF);
}


void write_register(uint8_t addr, uint8_t data) {
	uint16_t res;
	CTRL_PORT &= ~CSN_BIT;
	res = spi_transfer16( (data & 0x00FF) | (((addr & RF24_REGISTER_MASK) | RF24_W_REGISTER) << 8) );
	rf_status = (uint8_t) ((res & 0xFF00) >> 8);
	CTRL_PORT |= CSN_BIT;
}


/* Sets the address used for transmitting.
 */
void set_transmit_addr(uint8_t *addr) {
	int i;

	CTRL_PORT &= ~CSN_BIT;
	rf_status = spi_transfer(RF24_TX_ADDR | RF24_W_REGISTER);
	for (i = RF_ADDR_WIDTH - 1; i >= 0; i--) {
		spi_transfer(addr[i]);
	}
	CTRL_PORT |= CSN_BIT;
}


/* Sets the address used for receiving. The pipe number
 * cannot be greater than 5.
 */
void set_receive_addr(uint8_t pipe, uint8_t *addr) {
	int i;

	CTRL_PORT &= ~CSN_BIT;
	rf_status = spi_transfer((RF24_RX_ADDR_P0 + pipe) | RF24_W_REGISTER);
	if (pipe > 1) {  // Pipes 2-5 differ from pipe1's addr only in the LSB.
		spi_transfer(addr[RF_ADDR_WIDTH - 1]);
	} else {
		for (i = RF_ADDR_WIDTH - 1; i >= 0; i--) {
			spi_transfer(addr[i]);
		}
	}
	CTRL_PORT |= CSN_BIT;
}


/* Sets the data to be sent on the next transmission. The data is
 * written to the TX FIFO registers. The data length must be even.
 */
void set_transmit_data(uint8_t len, uint8_t *data) {
	int i;

	CTRL_PORT &= ~CSN_BIT;
	rf_status = spi_transfer(RF24_W_TX_PAYLOAD);
	for (i = 0; i < len; i += 2) {
		spi_transfer16((data[i] << 8) | (0x00FF & data[i+1]));
	}
	CTRL_PORT |= CSN_BIT;
}


/* Reads the contents of the RX FIFO registers. The data length must
 * be even.
 */
uint8_t receive_data(uint8_t len, uint8_t *dest) {
	uint16_t data;
	int i;

	CTRL_PORT &= ~CSN_BIT;
	rf_status = spi_transfer(RF24_R_RX_PAYLOAD);

	for (i = 0; i < len; i += 2) {
		data = spi_transfer16(0xFFFF);
		dest[i] = (data & 0xFF00) >> 8;
		dest[i+1] = (data & 0x00FF);
	}

	CTRL_PORT |= CSN_BIT;
	return ((rf_status & 0x0E) >> 1);  // return the pipe that this data belongs to
}


/* Flushes all of the TX FIFO registers.
 */
void flush_tx() {
	CTRL_PORT &= ~CSN_BIT;
	rf_status = spi_transfer(RF24_FLUSH_TX);
	CTRL_PORT |= CSN_BIT;
}


/* Flushes all of the RX FIFO registers.
 */
void flush_rx() {
	CTRL_PORT &= ~CSN_BIT;
	rf_status = spi_transfer(RF24_FLUSH_RX);
	CTRL_PORT |= CSN_BIT;
}


void pulse_ce() {
	CTRL_PORT |= CE_BIT;
	__delay_cycles(DELAY_CYCLES_15US);
	CTRL_PORT &= ~CE_BIT;
}


void nrf24_init() {
	spi_init();

	// Configure control ports and initialize outputs
	CTRL_DIR |= CSN_BIT + CE_BIT;
	CTRL_PORT |= CSN_BIT;
	CTRL_PORT &= ~CE_BIT;

	spi_transfer(RF24_NOP);

	__delay_cycles(1600000);  // wait 100ms for initialization

	close_all_pipes();
	write_register(RF24_RF_CH, (120 & 0x7F));  // set channel to 120
	write_register(RF24_SETUP_AW, (RF_ADDR_WIDTH - 2) & 0x03); // set address width to RF_ADDR_WIDTH

	CTRL_PORT &= ~CE_BIT;
	set_config(RF24_PWR_UP);

	flush_tx();
	flush_rx();
}


/* Closes the specified pipe. The pipe id shouldn't
 * exceed 5.
 */
void close_pipe(uint8_t pipeid) {
	uint8_t rxen, enaa;

	rxen = read_register(RF24_EN_RXADDR);
	enaa = read_register(RF24_EN_AA);

	rxen &= ~(1 << pipeid);
	enaa &= ~(1 << pipeid);

	write_register(RF24_EN_RXADDR, rxen);
	write_register(RF24_EN_AA, enaa);
}


void close_all_pipes() {
	write_register(RF24_EN_RXADDR, 0x00);
	write_register(RF24_EN_AA, 0x00);
	write_register(RF24_DYNPD, 0x00);
}


/* Opens a new pipe with Enhanced ShockBurst enabled.
 */
void open_pipe(uint8_t pipeid) {
	uint8_t rxen, enaa;

	rxen = read_register(RF24_EN_RXADDR);
	rxen |= (1 << pipeid);

	enaa = read_register(RF24_EN_AA);
	enaa |= (1 << pipeid);

	write_register(RF24_EN_RXADDR, rxen);
	write_register(RF24_EN_AA, enaa);
}


/* Sets the packet size of the specified pipe. The pipe id cannot
 * exceed 5 and the size cannot exceed 32.
 */
void set_pipe_size(uint8_t pipe, uint8_t size) {
	uint8_t config;

	config = read_register(RF24_DYNPD);
	config &= ~(1 << pipe);  // disable dynamic payload size
	write_register(RF24_RX_PW_P0 + pipe, size);
	write_register(RF24_DYNPD, config);
}


uint8_t set_config(uint8_t config) {
	uint8_t old_config;

	old_config = read_register(RF24_CONFIG);
	uint8_t irq_mask = ~(RF24_MASK_RX_DR | RF24_MASK_TX_DS | RF24_MASK_MAX_RT);
	write_register(RF24_CONFIG, ((RF24_EN_CRC | RF24_CRCO) | config) & irq_mask);
	return old_config;
}


void activate_receive() {
	flush_rx();
	write_register(RF24_STATUS, RF24_RX_DR);

	set_config(RF24_PWR_UP | RF24_PRIM_RX);
	CTRL_PORT |= CE_BIT;
}


void activate_transmit() {
	write_register(RF24_STATUS, RF24_TX_DS|RF24_MAX_RT);

	pulse_ce();
}


/* Checks if there is any data waiting in the RX FIFO registers.
 */
uint8_t data_available() {
	CTRL_PORT &= ~CSN_BIT;
	rf_status = spi_transfer(RF24_NOP);
	CTRL_PORT |= CSN_BIT;

	if ((rf_status & 0x0E) < 0x0E)
		return 1;
	return 0;
}
