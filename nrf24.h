#ifndef NRF24_H_
#define NRF24_H_

void nrf24_init();
void set_pipe_size(uint8_t pipe, uint8_t size);
void open_pipe(uint8_t pipeid);
void set_transmit_addr(uint8_t *addr);
void set_receive_addr(uint8_t pipe, uint8_t *addr);
void set_transmit_data(uint8_t len, uint8_t *data);
void activate_transmit();
void activate_receive();
uint8_t data_available();
uint8_t receive_data(uint8_t len, uint8_t *dest);



#endif /* NRF24_H_ */
