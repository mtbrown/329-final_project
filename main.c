#include <msp430.h>
#include <stdint.h>
#include <stdio.h>

#include "uart.h"
#include "dht.h"
#include "nrf24.h"


int role = 0;

char print_buf[256];
uint8_t data_buf[32];

int main(void) {
	WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
	if (CALBC1_16MHZ==0xFF) // If calibration constant erased
	{
		while(1); // do not load, trap CPU!!
	}


	DCOCTL = 0; // Select lowest DCOx and MODx settings
	BCSCTL1 = CALBC1_16MHZ; // Set range
	DCOCTL = CALDCO_16MHZ; // Set DCO step + modulation
	BCSCTL2 = DIVS_1;  // SMCLK = DCOCLK/2

	uart_init();
	nrf24_init();

	set_pipe_size(0, 32);
	open_pipe(0);

	if (role == 1) {
		transmit();
	} else {
		receive();
	}

	return 0;
}

void transmit() {
	char addr[5];

	addr[0] = 0xDE; addr[1] = 0xDE; addr[2] = 0xBE; addr[3] = 0xEF; addr[4] = 0x00;

	set_transmit_addr(addr);
	set_receive_addr(0, addr);

	P1DIR |= BIT0;
	P1OUT |= BIT0;
	CCTL0 = CCIE;
	CCR0 = 32768;
	TACTL = TASSEL_1 + MC_2;

	while (1) {
		uart_print("Reading...\r\n");
		dht_data data = dht_read();
		uart_print("Read finished\r\n");
		sprintf(print_buf, "%d, %d\r\n", data.humidity, data.temp);
		uart_print(print_buf);

		data_buf[0] = (data.humidity & 0xFF00) >> 8;
		data_buf[1] = (data.humidity & 0x00FF);
		data_buf[2] = (data.temp & 0xFF00) >> 8;
		data_buf[3] = (data.temp & 0x00FF);

		int humidity = (data_buf[0] << 8) | data_buf[1];
		sprintf(print_buf, "Humidity: %d\r\n", humidity);
		uart_print(print_buf);

		set_transmit_data(32, data_buf);
		activate_transmit();
		P1OUT &= ~BIT0;
		_BIS_SR(LPM0_bits + GIE);
	}
}

void receive() {
	char addr[5];
	int i;

	addr[0] = 0xDE; addr[1] = 0xDE; addr[2] = 0xBE; addr[3] = 0xEF; addr[4] = 0x00;

	set_receive_addr(0, addr);
	activate_receive();

	while (1) {
		if (data_available()) {
			for (i = 0; i < 32; i++) {
				data_buf[i] = 0;
			}

			uart_print("Receiving data\r\n");
			receive_data(32, data_buf);

			int humidity = (data_buf[0] << 8) | data_buf[1];
			int temp_c = (data_buf[2] << 8) | data_buf[3];
			int temp_f = (temp_c * 9) / 5 + 320;

			sprintf(print_buf, "Humidity: %d.%d%%, Temperature: %d.%d °C (%d.%d °F)\r\n",
					humidity / 10, humidity % 10, temp_c / 10, temp_c % 10,
					temp_f / 10, temp_f % 10);
			uart_print(print_buf);
		}
	}
}


#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A (void)
{
	P1OUT |= BIT0;
	CCR0 += 65536;
	_BIC_SR_IRQ(LPM0_bits);
}

