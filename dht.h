#ifndef DHT_H_
#define DHT_H_

#include <stdint.h>

typedef struct {
	uint16_t humidity;
	uint16_t temp;
} dht_data;

dht_data dht_read();

#endif /* DHT_H_ */
